import React, { Component, lazy, Suspense, useContext, useState, useEffect } from 'react'
import { Route, Link, BrowserRouter as Router, Switch } from 'react-router-dom'
import './App.scss';
import UserContext from './Context/UserContext';
// import useAuthentication from './Use/useAuthentication';
const DefaultWeb = lazy(() => import('./DefaultWeb/DefaultWeb'));
const AuthLayout = lazy(() => import('./Auth/AuthLayout'));
const UserPortal = lazy(() => import('./User/UserPortal'));

function App() {
  const [state, setState] = useState({
    user: {},
    isLogin: false
  })
  // const authUser = useAuthentication();
  const setUserData = (data) => {
    setState({
      ...state,
      isLogin: true
    })
    // alert('asd')
    // setUser(data)
    // console.log(authUser)

  }
  let value = {
    user: state.user,
    setUserData: setUserData,
  }
  return (
    <div className="App">
      <UserContext.Provider value={value}>
        <Router>
          <Suspense fallback={<div>Loading...</div>}>
            <Switch>
              {
                !state.isLogin ? <Route path="/" component={AuthLayout} /> : <Route path="/" component={UserPortal} />
              }
            </Switch>
          </Suspense>
        </Router>
      </UserContext.Provider>
    </div>
  )
}

export default App

