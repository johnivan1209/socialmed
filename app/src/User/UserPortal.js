import React from 'react'
import PropTypes from 'prop-types'
import { Route, Switch } from 'react-router-dom'
import PageHome from './PageHome'

function UserPortal(props) {
    return (
        <div>
            <Switch>
                <Route path="/" component={PageHome} />
            </Switch>
        </div>
    )
}
export default UserPortal

