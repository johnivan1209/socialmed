import React from 'react'

const UserContext = React.createContext({
    user: {},
    setUserData: () => { },
});

export default UserContext;