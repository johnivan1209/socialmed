export const Format = {
    ErrMessage
}

function ErrMessage(err) {
    return err.message.replace("Firebase", "Error");
}