import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import UserContext from '../Context/UserContext'
import InputAdaptive from '../PublicComponent/InputAdaptive'

function Login(props) {
    const user_context = useContext(UserContext)
    const afterLogin = (user) => {
        user_context.setUserData(user)
    }
    const loginUser = () => {
        props.history.push('/')
        afterLogin()
    }
    return (
        <form className="frnly_form" autoComplete="off">
            <InputAdaptive type="text" placeholder="Email or phone" />
            <InputAdaptive type="password" placeholder="Password" />
            <button className="btn btn-primary" style={{ width: "100%", height: "3rem" }} onClick={loginUser}>Login</button>
            <div className="frnly_hr" />
            <div style={{ textAlign: "center" }}>
                <Link className="btn btn-success" to="/signup">Create Account</Link>
            </div>
        </form>
    )
}

export default Login
