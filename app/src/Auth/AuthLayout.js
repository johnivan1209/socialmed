import React, { Component, useContext } from 'react'
import Login from './Login'
import Signup from './Signup'
import { Route, Switch } from 'react-router-dom'
import "./AuthLayout.scss"
import UserContext from '../Context/UserContext'
function AuthLayout() {
    return (
        <div className="frnly_app">
            <div className="contentwrapper">
                <div className="content">
                    <div className="frnly_left">
                        <div className="frnly_connect bolder">
                            <div className="frnly_logo_container">
                                <img className="frnly_logo" src="https://static.xx.fbcdn.net/rsrc.php/y8/r/dF5SId3UHWd.svg" alt="Facebook" />
                            </div>
                            <h3>Connect with friends and the
                                world around you on Frindly.</h3>
                        </div>
                    </div>
                    <div className="frnly_right">
                        <div className="frnly_form_content">
                            <Switch>
                                {/* <Route path="/" component={DefaultWeb} /> */}
                                <Route path="/signup" component={Signup} />
                                <Route path={["/", "/login"]} component={Login} />
                            </Switch>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    )
}

export default AuthLayout
