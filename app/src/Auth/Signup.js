import React, { useContext, useRef } from 'react'
import useInput from '../PublicComponent/InputAdaptive';
import { AuthService } from '../Services/AuthServices';
import UserContext from '../Context/UserContext'
import { Link } from 'react-router-dom';

function Signup(props) {
    const ref_form = useRef()
    const user_context = useContext(UserContext)
    const [emailPhone, emailPhoneInput] = useInput({ type: "text", placeholder: "Email or phone", required: true });
    const [userName, userNameInput] = useInput({ type: "text", placeholder: "User name", required: true });
    const [password, passwordInput] = useInput({ type: "password", placeholder: "Password", required: true });

    const afterLogin = (user) => {
        user_context.setUserData(user)
    }
    const createAccount = () => {
        props.history.push('/')
        afterLogin()
    }
    return (
        <div className="frnly_form" ref={ref_form}>
            {emailPhoneInput}
            {userNameInput}
            {passwordInput}
            <button className="btn btn-success" style={{ width: "100%", height: "3rem", marginBottom: "1.75rem" }} onClick={createAccount}>Create Account</button>
            <Link to="/" >Have Already a Account</Link>
        </div>
    )
}

export default Signup
