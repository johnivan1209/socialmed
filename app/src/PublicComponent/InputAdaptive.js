import React, { useState } from 'react'
import "./Styles/InputAdaptive.scss"

function useInput({ type, placeholder = "Place Holder", required = false /*...*/ }) {
    const [value, setValue] = useState("");
    const input = <InputAdaptive value={value} onChange={e => setValue(e.target.value)} type={type} placeholder={placeholder} required={required} />;
    return [value, input];
}
function InputAdaptive(props) {
    return (
        <div className="frnly_input_adaptive">
            <input {...props} />
        </div>
    )
}

export default useInput
