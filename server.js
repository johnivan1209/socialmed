
const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const fs = require('fs')
const app = express();

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server running on ${port}`))
